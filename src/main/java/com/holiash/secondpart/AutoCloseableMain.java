package com.holiash.secondpart;

public class AutoCloseableMain {
  public static void main(String[] args) {
    try (MyAutoCloseableClass myAutoCloseableClass = new MyAutoCloseableClass()) {
      System.out.println("In try with resources");
    } catch (MyException e) {
      System.out.println(e);
    }
  }
}
