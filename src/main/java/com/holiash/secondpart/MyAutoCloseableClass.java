package com.holiash.secondpart;

public class MyAutoCloseableClass implements AutoCloseable {

  @Override
  public void close() throws MyException {
    throw new MyException("In MyAutoCloseableClass close() method");
  }
}
