package com.holiash.firstpart.MenuEnum;

public enum MenuEnum {
    SHOW, SWITCH_ON, SWITCH_OFF, SORT, FIND, COUNT, EXIT
}
