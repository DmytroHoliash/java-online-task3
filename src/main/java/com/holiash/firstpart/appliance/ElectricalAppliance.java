package com.holiash.firstpart.appliance;

import java.util.Objects;

public abstract class ElectricalAppliance implements Comparable<ElectricalAppliance> {
    private boolean SwitchedOn;
    private int electricityUsage;
    private String make;

    public ElectricalAppliance(boolean SwitchedOn, int electricityUsage, String make) {
        this.electricityUsage = electricityUsage;
        this.SwitchedOn = SwitchedOn;
        this.make = make;
    }

    public int getElectricityUsage() {
        return electricityUsage;
    }

    public void setElectricityUsage(int electricityUsage) throws Exception {
        if (electricityUsage < 0) {
            throw new Exception("Electricity usage must be positive number!!!");
        }
        this.electricityUsage = electricityUsage;
    }

    public boolean isSwitchedOn() {
        return SwitchedOn;
    }

    public void switchOn() {
        if (!this.SwitchedOn) {
            this.SwitchedOn = true;
        }
    }

    public void switchOff() {
        if (this.SwitchedOn) {
            this.SwitchedOn = false;
        }
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    @Override
    public String toString() {
        return "is switched on = " + SwitchedOn +
                ", electricity usage(Wt) = " + electricityUsage +
                ", make = '" + make + '\'' +
                "}\n";
    }

    @Override
    public int compareTo(ElectricalAppliance o) {
        if (this.electricityUsage > o.getElectricityUsage()) {
            return 1;
        } else if (this.electricityUsage < o.electricityUsage) {
            return -1;
        } else {
            return this.make.compareTo(o.getMake());
        }
    }

}
