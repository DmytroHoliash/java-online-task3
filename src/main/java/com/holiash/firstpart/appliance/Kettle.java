package com.holiash.firstpart.appliance;

public class Kettle extends ElectricalAppliance {
    private double volume;

    public Kettle(boolean isSwitchedOn, int electricityUsage, String make, double volume) {
        super(isSwitchedOn, electricityUsage, make);
        this.volume = volume;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) throws Exception {
        if ((volume < 0.) || (volume > 3.)) {
            throw new Exception("Volume is in range from 0.0 to 3.0");
        }
        this.volume = volume;
    }

    @Override
    public String toString() {
        return "Kettle { " +
                " volume(L) = " + volume +
                ", " + super.toString();
    }
}
