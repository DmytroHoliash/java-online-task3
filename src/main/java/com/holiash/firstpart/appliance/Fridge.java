package com.holiash.firstpart.appliance;

public class Fridge extends ElectricalAppliance {
    private int temperatureInside;

    public Fridge(boolean isSwitchedOn, int electricityUsage, String make, int temperatureInside) {
        super(isSwitchedOn, electricityUsage, make);
        this.temperatureInside = temperatureInside;
    }

    public int getTemperatureInside() {
        return temperatureInside;
    }

    public void setTemperatureInside(int temperatureInside) throws Exception {
        if ((temperatureInside < -30) || (temperatureInside > 10)) {
            throw new Exception("Temperature must be between -30 and 10");
        }
        this.temperatureInside = temperatureInside;
    }

    @Override
    public String toString() {
        return "Fridge { " +
                "Temperature(C) = " + temperatureInside +
                ", " + super.toString();
    }
}
