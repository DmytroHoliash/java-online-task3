package com.holiash.firstpart.appliance;

public class WashingMachine extends ElectricalAppliance {
    private double maxLoad;

    public WashingMachine(boolean isSwitchedOn, int electricityUsage, String make, double maxLoad) {
        super(isSwitchedOn, electricityUsage, make);
        this.maxLoad = maxLoad;
    }

    public double getMaxLoad() {
        return maxLoad;
    }

    public void setMaxLoad(double maxLoad) throws Exception {
        if (maxLoad < 0.) {
            throw new Exception("Max load must be positive");
        }
        this.maxLoad = maxLoad;
    }

    @Override
    public String toString() {
        return "WashingMachine { " +
                "Max load (kg) = " + maxLoad +
                ", " + super.toString();
    }
}
