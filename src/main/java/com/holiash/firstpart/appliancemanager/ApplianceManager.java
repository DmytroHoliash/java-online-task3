package com.holiash.firstpart.appliancemanager;

import com.holiash.firstpart.appliance.ElectricalAppliance;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ApplianceManager {
    private List<ElectricalAppliance> applianceList;

    public ApplianceManager(List<ElectricalAppliance> applianceList) {
        this.applianceList = new ArrayList<>(applianceList);
    }

    public List<ElectricalAppliance> getApplianceList() {
        return applianceList;
    }

    public void setApplianceList(List<ElectricalAppliance> applianceList) {
        this.applianceList = new ArrayList<>(applianceList);
    }

    public int countTotalElectricityUsage() {
        int total = 0;
        for (ElectricalAppliance appliance : this.applianceList) {
            if (appliance.isSwitchedOn()) {
                total += appliance.getElectricityUsage();
            }
        }
        return total;
    }

    public void sort() {
        this.applianceList.sort(ElectricalAppliance::compareTo);
    }

    public void switchOnAppliance(int index) {
        this.applianceList.get(index).switchOn();
    }

    public void switchOnAppliance(int begin, int end) {
        for (int i = begin; i < end; i++) {
            this.applianceList.get(i).switchOn();
        }
    }

    public void switchOffAppliance(int index) {
        this.applianceList.get(index).switchOff();
    }

    public void switchOffAppliance(int begin, int end) {
        for (int i = begin; i < end; i++) {
            this.applianceList.get(i).switchOff();
        }
    }

    public ElectricalAppliance findBy(int electricityUsageBegin, int electricityUsageEnd) {
        for (ElectricalAppliance appliance : this.applianceList) {
            if ((appliance.getElectricityUsage() > electricityUsageBegin)
                    && appliance.getElectricityUsage() < electricityUsageEnd) {
                return appliance;
            }
        }
        throw new NoSuchElementException("No elements in this range");
    }

    public ElectricalAppliance findBy(int electricityUsageBegin, int electricityUsageEnd, String make) {
        for (ElectricalAppliance appliance : this.applianceList) {
            if ((appliance.getElectricityUsage() > electricityUsageBegin)
                    && (appliance.getElectricityUsage() < electricityUsageEnd)
                    && appliance.getMake().equals(make)) {
                return appliance;
            }
        }
        throw new NoSuchElementException("No elements in this range");
    }
    public ElectricalAppliance findBy(String make) {
        for (ElectricalAppliance appliance : this.applianceList) {
            if (appliance.getMake().equals(make)) {
                return appliance;
            }
        }
        throw new NoSuchElementException("No elements of this make");
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (ElectricalAppliance appliance : this.applianceList) {
            sb.append(appliance.toString());
        }
        return sb.toString();
    }
}
