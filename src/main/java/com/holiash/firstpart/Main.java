package com.holiash.firstpart;

import com.holiash.firstpart.MenuEnum.MenuEnum;
import com.holiash.firstpart.appliance.ElectricalAppliance;
import com.holiash.firstpart.appliance.Fridge;
import com.holiash.firstpart.appliance.Kettle;
import com.holiash.firstpart.appliance.WashingMachine;
import com.holiash.firstpart.appliancemanager.ApplianceManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Main {
  static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

  public static void main(String[] args) throws IOException {
    List<ElectricalAppliance> list = new ArrayList<>();
    list.add(new Fridge(true, 15, "LG", -7));
    list.add(new Kettle(false, 10, "LG", 1.5));
    list.add(new WashingMachine(true, 25, "Electrolux", 7.5));
    list.add(new Kettle(true, 15, "Samsung", 2.0));
    ApplianceManager manager = new ApplianceManager(list);
    menu(manager);
  }

  static void menu(ApplianceManager manager) throws IOException {
    MenuEnum menu = MenuEnum.SHOW;
    showMenuOptions();
    Outer:
    while (true) {
      try {
        menu = MenuEnum.valueOf(br.readLine().toUpperCase());
      } catch (IllegalArgumentException e) {
        System.out.println(e.getMessage());
      }
      switch (menu) {
        case SHOW:
          System.out.println(manager);
          break;
        case COUNT:
          System.out.println(manager.countTotalElectricityUsage());
          break;
        case SORT:
          manager.sort();
          System.out.println("Successfully sorted");
          break;
        case SWITCH_ON:
          System.out.println("Input value(s): ");
          String[] valuesToSwitchOn = br.readLine().split(" ");
          try {
            if (valuesToSwitchOn.length == 1) {
              manager.switchOnAppliance(Integer.parseInt(valuesToSwitchOn[0]));
            } else if (valuesToSwitchOn.length == 2) {
              manager.switchOnAppliance(Integer.parseInt(valuesToSwitchOn[0]), Integer.parseInt(valuesToSwitchOn[1]));
            }
          } catch (IndexOutOfBoundsException ex) {
            System.out.println(ex.getMessage());
          }
          break;
        case SWITCH_OFF:
          System.out.println("Input value(s): ");
          String[] valuesToSwitchOff = br.readLine().split(" ");
          try {
            if (valuesToSwitchOff.length == 1) {
              manager.switchOffAppliance(Integer.parseInt(valuesToSwitchOff[0]));
            } else if (valuesToSwitchOff.length == 2) {
              manager.switchOffAppliance(Integer.parseInt(valuesToSwitchOff[0]), Integer.parseInt(valuesToSwitchOff[1]));
            } else {
              System.out.println("Wrong values");
            }
          } catch (IndexOutOfBoundsException ex) {
            System.out.println(ex.getMessage());
          }
          break;
        case FIND:
          System.out.println("Input parameters: ");
          String[] params = br.readLine().split(" ");
          try {
            if (params.length == 1) {
              System.out.println(manager.findBy(params[0]));
            } else if (params.length == 2) {
              System.out.println(manager.findBy(Integer.parseInt(params[0]), Integer.parseInt(params[1])));
            } else if (params.length == 3) {
              System.out.println(manager.findBy(Integer.parseInt(params[0]), Integer.parseInt(params[1]), params[2]));
            } else {
              System.out.println("Wrong params");
            }
          } catch (NoSuchElementException e) {
            System.out.println(e.getMessage());
          }
          break;
        case EXIT:
          break Outer;
        default:
          System.out.println("Wrong choice");
          break;
      }
    }
  }

  static void showMenuOptions() {
    System.out.println("Show\nSwitch_on\nSwitch_off\nSort\nFind\nCount\nExit");
  }

  static void clearScreen() {
    System.out.print("\033[H\033[2J");
    System.out.flush();
  }
}
